import {ApplicationService, Args} from "@themost/common";
import i18n from 'i18n';
import {set, merge} from 'lodash';

export class TranslateService extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        this.options = Object.assign({
            locales: ['en'],
            defaultLocale: 'en',
            directory: path.resolve(process.cwd(), 'locales'),
            autoReload: false,
            updateFiles: false,
            syncFiles: false,
            objectNotation: true
        }, this.getApplication().getConfiguration().getSourceAt('settings/i18n'));
        i18n.configure(this.options);
        this.defaultLocale = this.options.defaultLocale;
    }

    /**
     * Translates the given phrase by getting the default application locale
     * @param {string} phrase
     * @param {*=} params
     */
    translate(phrase, params) {
        return this.get(phrase, params);
    }

    /**
     * Translates the given phrase by getting the default application locale
     * Alias of TranslateService.translate(string, any) method
     * @param {string} phrase
     * @param {*=} params
     */
    get(phrase, params) {
        const str = i18n.__({
            locale: this.defaultLocale,
            phrase: phrase
        }, params);
        if (str == null) {
            return phrase;
        }
        return str;
    }

    /**
     * Sets a translation key for given locale (or the default locale)
     * @param {string} phrase
     * @param {string} translation
     * @param {string} locale
     */
    set(phrase, translation, locale) {
        if (this.options.locales.indexOf(locale) < 0) {
            throw new Error('The specified locale is unavailable.')
        }
        let catalog = i18n.getCatalog(locale);
        if (catalog === false) {
            throw new Error('The specified locale catalog is unavailable.');
        }
        catalog = i18n.getCatalog(locale);
        set(catalog, phrase, translation);
        
    }

    /**
     *
     * @param {string} locale
     * @param {*} translations
     */
    setTranslation(locale, translations) {
        let catalog = i18n.getCatalog(locale);
        if (catalog === false) {
            throw new Error('The specified locale catalog is unavailable.');
        }
        merge(catalog, translations);
    }

}
