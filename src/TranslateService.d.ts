import {ApplicationService, IApplication} from "@themost/common";
export declare class TranslateService extends ApplicationService {
    
    defaultLocale: string;

    constructor(app: IApplication);
translate(phrase: string, params?: any): string;
    get(phrase: string, params?: any): string;
    set(phrase: string, translation: string, locale: string): void;
    setTranslation(locale: string, translations: any): void;

}