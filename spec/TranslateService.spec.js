import { DataApplication } from '@themost/data';
import {TranslateService} from '../src/TranslateService';
import path from 'path';

describe('TranslateService', () => {

    let app;
    beforeAll(() => {
        app = new DataApplication(__dirname);
        app.getConfiguration().setSourceAt('settings/i18n', {
            directory: path.resolve(__dirname, 'locales')
        })
    });

    it('should create instance', async () => {
        const service = new TranslateService(app);
        expect(service).toBeTruthy();
    });

    it('should get translation', async () => {
        const service = new TranslateService(app);
        service.defaultLocale = 'en';
        service.translate('HelloString');
        expect(service.translate('HelloString')).toEqual('HelloString');
        service.setTranslation('en', {
            'HelloString': 'Hello World!'
        });
        expect(service.translate('HelloString')).toEqual('Hello World!');
    });

});