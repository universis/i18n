import { DataEventArgs } from "@themost/data";

export declare function beforeSave(event: DataEventArgs, callback: (err?: Error) => void): void;
export declare function afterSave(event, callback: (err?: Error) => void): void;